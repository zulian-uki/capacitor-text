import React from 'react';
import './ExploreContainer.css';
import { Plugins } from '@capacitor/core';

const { Browser } = Plugins;

interface ContainerProps {
  name: string;
}

const ExploreContainer: React.FC<ContainerProps> = ({ name }) => {
  return (
    <div className="container">
      <strong>{name}</strong>
      <p>Explore <span onClick={async () =>{
        await Browser.open({ url: 'http://capacitorjs.com/' });
      }}>UI Components</span></p>
    </div>
  );
};

export default ExploreContainer;
