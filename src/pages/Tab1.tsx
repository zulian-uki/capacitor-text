import React, { useState, useEffect } from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab1.css';
import { Plugins } from '@capacitor/core';

const { Device } = Plugins;

const Tab1: React.FC = () => {
  const [platform, setPlatform] = useState('');
  useEffect(() => {
    console.log("RUN EFFECT");
    Device.getInfo().then((v) => {
      console.log(v);
      setPlatform(v.platform);
    });
  })
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 1 {platform}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 1{platform}</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ExploreContainer name="Tab 1 page" />
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
